function growth
%GROWTH Summary of this function goes here
%   Detailed explanation goes here

pars = [.4,1000];
Init = 5;


t =[0.0,0.01,40.0];
options = odeset('RelTol',1e-8,'AbsTol',1e-8);
y = ode15s(@rhs,t,Init,options,pars);
x = deval(y,t);

figure(1);
h=plot(t,x);   

function dN = rhs(t,Init,pars)
r = pars(1);
K = pars(2);

N = Init(1);
dN = [r*N*((K-N)/K)];

