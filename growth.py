#!/usr/local/bin/python
from __future__ import division
import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
class rhs:
    def __init__(self,pars):
        self.pars = pars

    def __call__(self,Init,t):
        r = self.pars[0]
        K = self.pars[1]

        N = Init[0]
        return [r*N*((K-N)/K)]


pars = [.4,1000]
Init = [5]
f = rhs(pars)

t = np.linspace(0.0,40.0,num=1000)
y = (scipy.integrate.odeint(f,Init,t))
plt.plot(t,y)
plt.show()
